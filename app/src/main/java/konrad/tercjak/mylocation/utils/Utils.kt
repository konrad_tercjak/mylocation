package konrad.tercjak.mylocation.utils

import android.widget.TextView

    fun TextView.getIntOrNull():Int?{
        return text.toString().toIntOrNull()
    }
    fun TextView.getLongOrNull():Long?{
        return text.toString().toLongOrNull()
    }

    inline fun <R> R?.orElse(block: () -> R): R {
        return this ?: block()
    }
