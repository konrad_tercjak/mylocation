package konrad.tercjak.mylocation

import android.Manifest

import android.content.*
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.net.Uri
import android.os.Bundle

import android.provider.Settings
import android.util.Log
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import kotlinx.android.synthetic.main.activity_main.*
import konrad.tercjak.mylocation.utils.*

class MainActivity : AppCompatActivity(), GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {

    private val TAG = "MainActivity"

    private lateinit var googleApiClient: GoogleApiClient
    private var locationRequest: LocationRequest? = null
    lateinit var locationManager: LocationManager


    var targetLocation: Location? = null
    var signalDistance: Long = 100L
    var phoneNumber: String = ""
    var minRefreshRate: Long = 10000L
    var refreshRate: Long = 10000L


    fun writeState() {
        val sharedpreferences = getSharedPreferences("mylocation8", MODE_PRIVATE)

//        Log.e("Write prefs", "ddd")
        val edit = sharedpreferences.edit()
        val loc = targetLocation
        if (loc != null) {
            edit.putBoolean("hasData", true)
            edit.putString("lat", loc.latitude.toString())
            edit.putString("long", loc.longitude.toString())
        }


        edit.putLong("signalDistance", signalDistance)
        edit.putLong("minRefreshRate", minRefreshRate)
        edit.putString("phoneNumber", phoneNumber)
        edit.commit()

    }

    fun readState() {
//        Log.e("Read file", "ds")
        val sharedpreferences = getSharedPreferences("mylocation8", MODE_PRIVATE)

        val location = Location("a")
        val lat = sharedpreferences.getString("lat", "0")
        val long = sharedpreferences.getString("long", "0")

        location.latitude = lat.toDouble()
        location.longitude = long.toDouble()
        targetLocation = location
        latitudeTV.setText(lat)
        longitudeTV.setText(long)

        signalDistance = sharedpreferences.getLong("signalDistance", 100L)
        minRefreshRate = sharedpreferences.getLong("minRefreshRate", 1000L)
        phoneNumber = sharedpreferences.getString("phoneNumber", "666")//.orEmpty()

        phoneNumberTV.setText(phoneNumber)
        signalDistanceTV.setText(signalDistance.toString())
        refreshRateTV.setText(minRefreshRate.toString())
    }


    override fun onStart() {
        super.onStart()

        if (googleApiClient != null) {
            googleApiClient.connect()
        }
    }

    override fun onStop() {
        super.onStop()
        if (googleApiClient.isConnected) {
            googleApiClient.disconnect()
        }
        LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient,this)
        locationRequest=null
    }

    override fun onPause() {
        super.onPause()

        if (googleApiClient.isConnected) {
            googleApiClient.disconnect()
        }

        if (mManager != null) {
            mManager.removeUpdates(this);
        }
        
        LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient,this)
        locationRequest=null
    }

    override fun onConnectionSuspended(p0: Int) {

        Log.i(TAG, "Connection Suspended")
        googleApiClient.connect()
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        Log.i(TAG, "Connection failed. Error: " + connectionResult.errorCode)
    }

    override fun onLocationChanged(location: Location) {

        val msg = """Updated location: latitude ${location.latitude} longitude +${location.longitude}"""
        latitudeText.text = "" + location.latitude
        longitudeText.text = "" + location.longitude
        createToast(msg)


        targetLocation?.let {
            var distance: Float = it.distanceTo(location)
//            createToast((distance/location.speed).toString())

            if (distance <= signalDistance-location.accuracy*2) {

                distanceInfoLabel.apply {
//                    setTextColor(Color.GREEN)
                    text = "%.0f".format(distance) + "m (HA "+location.accuracy+ "m)"
                }

              LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient,this)
//                Handler().postDelayed({ startDial() }, 2000)
                startDial()
            }
//Gdy bieżąca odległość jest:
//distance < 1km pokazujemy ją na czerwono, w przeciwnym razie na niebiesko
//distance> 5km pokazujemy wartość w pełnych km (zaokrąglona w górę)
//distance> 1km i distance<= 5km pokazujemy wartość w km z precyzją jednego miejsca po przecinku
//distance<= 1km pokazujemy wartość w metrach

            else {
                if(location.speed>0) {
                    val time = distance / location.speed * 1000
                    if (time < minRefreshRate) {
                        refreshRate = time.toLong()
                        LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this)
                        startLocationUpdates(refreshRate, refreshRate)
                    }
                }

//                val color = if (distance < 1000) Color.RED else Color.BLUE
                with(distanceInfoLabel) {
//                    setTextColor(color)

                    if (distance <= 1000) {
                        text == "%.0f".format(distance) + " m"
                    } else {
                        distance /= 1000
                        if (distance <= 5) {
                            text = "%.1f".format(distance) + " km"
                        } else {
                            text = "%.0f".format(distance) + " km"
                        }
                    }


                }

            }


        }
    }

    override fun onConnected(p0: Bundle?) {

        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {

            return
        }


        startLocationUpdates(minRefreshRate, minRefreshRate)

        var fusedLocationProviderClient:
                FusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)

        fusedLocationProviderClient.lastLocation
            .addOnSuccessListener(this) { location ->
                // Got last known location. In some rare situations this can be null.
                location?.let {
                    latitudeText.text = "" + location.latitude
                    longitudeText.text = "" + location.longitude
                }

            }
    }


    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setGoogleApiClient()

        locationManager = this.getSystemService(Context.LOCATION_SERVICE) as LocationManager

        setListeners()

        if (!isLocationEnabled()){
            showEnableLocationAlert()
        }
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        readState()

    }
    private fun setGoogleApiClient(){
        googleApiClient = GoogleApiClient.Builder(this)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .addApi(LocationServices.API)
            .build()

    }



    private fun isLocationEnabled(): Boolean {
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }

    private fun showEnableLocationAlert() {
        val dialog = AlertDialog.Builder(this)
        dialog.setTitle("Enable Location")
            .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " + "use this app")
            .setPositiveButton("Location Settings", DialogInterface.OnClickListener { paramDialogInterface, paramInt ->
                val myIntent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(myIntent)
            })
            .setNegativeButton("Cancel", DialogInterface.OnClickListener { paramDialogInterface, paramInt -> })
        dialog.show()
    }

    protected fun startLocationUpdates(updateInterval: Long = 2000L, fastestInterval: Long = 2000L) {

        // Create the location request
        locationRequest = LocationRequest.create()
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
            .setInterval(updateInterval)
            .setFastestInterval(fastestInterval)
        // Request location updates
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(
            googleApiClient,
            locationRequest, this
        )
    }

    private fun createToast(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT)
            .show()
    }


    private fun setListeners() {

        saveButton.setOnClickListener {

            val targetLat = latitudeTV.text.toString().toDoubleOrNull()
            val targetLong = longitudeTV.text.toString().toDoubleOrNull()


            if (targetLat != null && targetLong != null) {
                targetLocation = Location("point A").apply { latitude = targetLat; longitude = targetLong }
            } else {
                createToast("Wrong Location")
            }

            phoneNumberTV.getIntOrNull()?.let {
                phoneNumber= it.toString()
            }.orElse {
                createToast("Wrong phone number")
            }

            signalDistanceTV.getLongOrNull()?.let {
                signalDistance = it
            }.orElse {
                createToast("Wrong distance to signal")
            }

            refreshRateTV.getLongOrNull()?.let {
                writeState()
                startLocationUpdates(minRefreshRate, minRefreshRate)
            }.orElse {
                createToast("Empty refresh rate")
            }

        }

        setCurrentButton.setOnClickListener {
            latitudeTV.setText(latitudeText.text)
            longitudeTV.setText(longitudeText.text)
        }

    }

    fun startDial() {

        if (!phoneNumber.equals("")) {

            val number = Uri.parse("tel:" + phoneNumber)
            val dial = Intent(Intent.ACTION_DIAL, number)
            startActivity(dial)
            closeApp()

        }

    }

    fun closeApp() { finish() }


}
